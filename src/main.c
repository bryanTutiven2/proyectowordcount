
#include <stdio.h>
#include <getopt.h>
#include "../include/funciones.h"
#include <string.h>

int main (int argc, char *argv[]){
	bool ArValido;
	int opt;
	char *nombre_archivo;
	//printf("%i\n", argc );

	//muestra el menu help si se usa el comando -h solamente
	if( argc>1 && strcmp(argv[1],"-h") == 0){
		func_help();
			return -1;
	}
	
	//infica el uso del comando cuando hay parametros insuficientes
	 if (argc < 2 || argc > 4) {
        fprintf(stderr, "Uso: %s COMANDO ARCHIVO  ...\n", argv[0]);
        func_help();
        return -1;
    }
    //si son solo 2 argumentos y si el archivo es valido mostrara todos los datos 
    //posible sobre el archivo
    if(argc == 2){
		nombre_archivo = argv[1];
		bool valido =  validacionArchivo(nombre_archivo);
		if(valido == true)
			printf("%i  %i  %i %s \n", func_salto_linea(nombre_archivo),func_conteo_palabras(nombre_archivo),
				func_conteo_caracteres(nombre_archivo),nombre_archivo);
	}
	//verifica que existan 3 valores ingresados por consola 
	//valida el el archivo y da la posibilidad de entrar al switch
	//si el numero de argumento es 4 se ingresa permite el paso a una funcion que permite encontrar una palabra en el archivo
    if(argc == 3 || argc == 4){
    	nombre_archivo = argv[argc-1];
		ArValido =  validacionArchivo(nombre_archivo);

    }

    //si el archivo es valido permite utilizar los comandos 
	if(ArValido ==true){
		while ((opt = getopt (argc, argv, "h:l:w:c:f")) != -1){
			switch(opt){
				case 'h':
					func_help();
					break;
				case 'l':
					printf("%i\t%s\n",func_salto_linea(nombre_archivo),nombre_archivo);
					break;
				case 'w':
					
					printf("%i\t%s\n",func_conteo_palabras(nombre_archivo), nombre_archivo);
					break;
				case 'c':
					printf("%i\t%s\n",func_conteo_caracteres(nombre_archivo),nombre_archivo );
					break;
				case 'f':
					func_encontrar_palabra(nombre_archivo, argv[2]); //funcion manda el nombre del archivo previamente evaluado junto a la palabra a buscar
					break;
				case '?':
					fprintf(stderr, " Option: %s desconocida \n",argv[1]);
					func_help();
					break;
				default:
					printf("Error \n");
			}
		}
	}
	
}
