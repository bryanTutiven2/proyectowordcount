#include "../include/funciones.h"
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#define MAX 50
//funcion de validacionArchivo permite saber si el nombre del archivo incresado esta o no para trabajar
bool validacionArchivo(char *nombre){
	FILE *fd;
	fd = fopen(nombre,"rt");
	if(fd ==NULL){
		fprintf(stderr,"ERROR EL ARCHIVO NO EXISTE \n");
		return false;
	}else{
		return true;
	}

};

//Funcion help lanza un mensaje con las opciones que esta disponible para el comando word count o wc
void func_help(){

	printf("wc [option] FILE \nwc, o \"word count,\" muestra en consola el número \nde saltos de línea, palabras, y caracteres del archivo de entrada.\n \t-h: ayuda, muestra este mensaje\n \t-l: muestra el total de saltos de línea\n \t-w: muestra el total de palabras\n \t-c: muestra el total de caracteres\n \t-f Buscar una palabra, retorna en que numero de linea esta la palabra\n");

}
//Esta funcion permite comparar el final de cada linea si es un salto de linea
int func_salto_linea(char *nombre){
	FILE *fd;
	int c;
	fd = fopen(nombre,"rt"); 
	int saltos =0;
	while((c=fgetc(fd))!= EOF){
		if(c == '\n'){
			saltos+=1;
		}
	}
	return saltos;
}

//Conteo de Palabras permite saber cuantas palabras estan presente en el archivo
int func_conteo_palabras(char *nombre){
	FILE *fd;
	fd = fopen(nombre,"rt");
	int palabras =0;
	char palabra_actual[MAX];
	 	while ((fscanf(fd, "%s", palabra_actual)) != EOF){ //fscanf lee secuencias de entradas provista por el archivo
		 	if(strcmp(palabra_actual,"\0") !=0){ //strcmp funcion de la libreria string.h permite comparar dos cadenas
	        		palabras+=1;
	        	} 
       	}
	return palabras;
}

// La funcion siguiente cuenta los caracteres que se encuentran el archivo por ello se utliza EOF que permite evaluar el fin 
int func_conteo_caracteres(char *nombre){
	FILE *fd;
 	int c;
	fd = fopen(nombre,"rt");
	int caracteres = 0 ; 
	while((c=fgetc(fd))!= EOF){	
		caracteres+=1;	
		}
	return caracteres;
}

//Funcion de encontrar palabra es una funcion extra que se deseo crear, esta permite buscar una palabra en una linea o 
//en su efecto dentro de una cadena.
//La funcion devuelve en que numero de linea se encuentra la palbra a buscada
//Imprimi error en caso de no encontrar la palabra
//En caso de que exista la palabra el ciclo terminara en cuanto la encuentre sin importar que existan en otras lineas
void func_encontrar_palabra(char *nombre, char *palabra){
	char linea[MAX];
    char *p;
    FILE *fd;
    fd = fopen(nombre,"rt");
    int i=0;
    int w_encontrada=0;
    char temp[MAX];
    fgets(linea,MAX,fd);
    while(!feof(fd)) {
       	i++;
       	if(p=strstr(linea,palabra)) {
       		w_encontrada++;
	       	sscanf(p,"%s",temp);/*si se encuentra la palabra, sscanf almacena en temp (temporal) la palabra que contiene en una sub palabra*/
        }
        if(!strcmp(temp,palabra)) /* comparo las palabras, y en caso de ser iguales sale del ciclo while */
            break;
       fgets(linea,MAX,fd);
    }
    if(w_encontrada>0)
    	printf("Palabra \'%s\' en la linea: %d\n",palabra,i);
    else if(w_encontrada == 0) //en caso de que no se haya encontrado la palabra se retorna el sgte fprintf
    	fprintf(stderr, "Palabra \'%s\' no encontrada\n", palabra);
}



