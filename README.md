# ProyectoWordCount
 información de los integrantes e instrucciones relevantes.
Integrantes 
Bryan Tutiven
Juan Jimenez

** Indicaciones 
La funcion buscar palabra usa ./wc -f palabra archivo
funciones 
./wc archivo muestra todos los datos del archivo
./wc -l Leer.txt muestra las lineas dentro de un archivo
./wc -c total de caracteres dentro del archivo
./wc -h ayuda, muestra como usar el comando
./wc -w cuenta las palabras dentro del archivo


*retorna la primera coincidencia de la palabra en el texto.

Proyecto distribuido en directorios/ mejora el control de versiones y errores / las funciones que usa el proyecto estan dentro de un archivo funciones.c

Texto a usar para el ejemplo
---------------------------------------------------
I walked across an empty land
I knew the pathway like the back of my hand
I felt the earth beneath my feet
Sat by the river, and it made me complete

Oh, simple thing, where have you gone?
I'm getting old, and I need something to rely on
So tell me when you're gonna let me in
I'm getting tired, and I need somewhere to begin
----------------------------------------------------
