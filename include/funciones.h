#ifndef estructura_A
#define estructura_A
#include <stdbool.h> 
#define MAXSTR 50

void func_help();
bool validacionArchivo(char *nombre);
int func_salto_linea(char *nombre);
int func_conteo_palabras(char *nombre);
int func_conteo_caracteres(char *nombre);
void func_encontrar_palabra(char *nombre, char *palabra);
#endif