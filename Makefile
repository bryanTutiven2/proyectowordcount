
main_c= ./src/main.c
funciones_c= ./src/funciones.c
funciones_h= ./include/funciones.h

wc: main.o funciones.o 
	gcc -o wc main.o funciones.o 
main.o : $(main_c) $(funciones_h)
	gcc -c $(main_c)
funciones.o : $(funciones_c) $(funciones_h) 
	gcc -c $(funciones_c)
clean:
	rm -f wc *.o
